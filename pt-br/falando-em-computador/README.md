# Falando em Computador

A tecnologia da computação faz parte do nosso dia-a-dia em nossos celulares, computadores, televisores, _tablets_, relógios, ônibus, carros, geladeiras... Até mesmo em roupas!

Cada um desses diferentes dispositivos possuem tecnologias próprias para seu uso e ambiente de atuação. Celulares precisam possuir boa câmera, uma _interface_ bonita e de fácil uso para uma mão; relógios precisam ser leves e sua _interface_ minimalista para uma tela que cabe em seu pulso; Carros precisam ser seguros para não causar defeitos que acarretariam em acidentes.

Entretanto, todos estes componentes compartilham de princípios no que tange ao processo de computação e comunicação. Por isso, o primeiro passo para o compreender o mundo da tecnologia é entender como funciona um computador.

Com este objetivo, esta seção trata dos seguintes temas:

1. [O que é _Hardware_?](#o-que-é-hardware)
1. [Componentes de um Computador](#componentes-de-um-computador)
1. [Dispositivos de Entrada e Saída](#dispositivos-de-entrada-e-saída)
1. [Armazenamento e Memória](#armazenamento-e-memória)
1. [Processador](#processador)
1. [O que é _Software_?](#o-que-é-software)
1. [Arquivo, Programa e Processo](#arquivo-programa-e-processo)

## O que é _Hardware_?

_Hardware_ são os componentes físicos de uma máquina computacional. É aquilo que você pode tocar.

* Em seu celular: a tela é um _hardware_, a câmera é um _hardware_ e até mesmo a capa protetora é um _hardware_.
* Em um computador: o mouse é um _hardware_, o monitor é um _hardware_, o gabinete é, pasmem, um _hardware_! Todos os componentes internos contidos no gabinete também são _hardwares_.

> Ok, ok. Então se tudo é _hardware_, o que diabos não é _hardware_?

Tudo aquilo que não faz parte de uma máquina computacional não é um _hardware_. Seu calçado (até onde sei) não é um _hardware_ pois não é parte de uma máquina computacional. Um travesseiro (comum) também não é.

(**Nota**: A parte mais interessante do parágrafo acima é o receio de descrever coisas que não são _hardwares_, pois existem [travesseiros](https://www.youtube.com/watch?v=mi17whiEo44) e [calçados](https://www.youtube.com/watch?v=pTIZZ3QoG5Q) com computadores embutidos, fazendo-o estes partes de uma máquina computacional!)

Tudo aquilo que faz parte de uma máquina computacional, mas é uma abstração lógica resultante de operações realizadas por _hardwares_ é chamado de _software_. _Softwares_ não são _hardwares_.

O nome está relacionado com a capacidade de transformação, modificação ou adaptação de um componente computacional. _Hardwares_ são rígidos, imutáveis ou de difícil mutabilidade, construídos para realizar sua tarefa e apenas essa tarefa da forma que foi construída para fazer. Para modificar um _hardware_, possivelmente você terá que modificar suas peças e/ou construir um novo.

## Componentes de um computador

Um computador típico possui os seguintes componentes:

* [**Placa-Mãe**](https://www.youtube.com/watch?v=UhE3SJw8XW4) (_motherboard_): Componente responsável por integrar outros componentes em estruturas padronizada de encaixes e portas.

    * Celulares costumam ter placas-mãe que integram chips de telefones, processador, bateria, _touchscreen_, câmera, antena e cartão de memória.
    * Computadores costumam ter placas-mãe que integram dispositivos USBs (como teclado, mouse e pen drive), placa de vídeo, placa de aúdio, placa de rede, processador, placas de memória RAM, discos rígidos e fonte de energia.
* [**Processador**](https://www.youtube.com/watch?v=YE7-H_w2GAY): Componente responsável por interpretar instruções recebidas em processos de diferentes dispositivos conectados em seu sistema.

    * Esses processos são criados através da conversão de dados em memória (ou de tarefas pré-definidas em um _hardware_) em formato binário (0 e 1) para uma série elétrica correspondente que é transmitida para o processador.
    * A informação de valor 0 corresponde à uma ausência de energia elétrica, enquanto a informação de valor 1 corresponde à presença de energia elétrica.
* **Memória**: Componente capaz de armazenar informações.

    * O tipo de memória que consegue armazenar a informação permanentemente (isto é, sem energia vinculada ao _hardware_) é chamada de "[perene](https://www.youtube.com/watch?v=StrNfD9p86M)" ou "não-volátil". O disco rígido em um computador é um exemplo de memória perene, assim como o componente de armazenamento em um _pen drive_.
    * O tipo de memória que perde a informação quando não há energia é chamada de "[volátil](https://www.youtube.com/watch?v=TtpOVyhIiP8)". Memória RAM é um exemplo de memória volátil. A memória volátil é mais rápida e mais cara que a memória perene.
* **Periféricos**: Componentes que inserem informações para o sistema e/ou transmitem informações para outros sistemas e/ou para o usuário.

    * Monitor é um exemplo de um periférico de saída (Informações saem do computador para o monitor).
    * _Mouse_ é um exemplo de um periférico de entrada (Informações do _mouse_ são transmitidas para o computador).
    * _Touchscreen_ é um exemplo de um periférico de entrada e saída (Informações saem do celular para o _touchscreen_ e vice-versa).

## Dispositivos de Entrada e Saída

O conceito de "Entrada" (_Input_) e "Saída" (_Output_) é abordado em diversos contextos da tecnologia da informação (TI). Um computador comum é montado através da integração de diversos dispositivos, cada qual com seus objetivos.

Diversos produtos são disponibilizados por empresas de tecnologia. Para ser capaz de integrar tais produtos em uma máquina se faz necessário de _interfaces_ padronizadas.

_Interface_ é outro termo bastante utilizado em TI, geralmente definindo alguma abordagem para acoplar diferentes módulos, dispositivos, códigos ou dados.

Dispositivos de entrada são aqueles que inserem informações no computador advindo de um meio externo. Este meio externo pode ser uma pessoa (utilizando de um teclado, por exemplo), um outro dispositivo do próprio computador (como um sensor de temperatura no computador) ou um outro computador (como informações advindo da internet, recebidos por uma placa de rede).

Dispositivos de saída são aqueles que emitem informações do computador para um meio externo. Este dispositivo pode ser destinado para pessoas (como um monitor que renderiza informações gráficas para o usuário), para outros dispositivos presentes no computador (como um pen drive inserido em uma porta USB) ou outros computadores (como o uso da internet através de uma placa de rede).

Dispositivos de entrada podem também ser dispositivos de saída. Uma placa de rede é capaz de transmitir informações para o meio externo e também receber informações de um meio externo. O _touchscreen_ renderiza informações gráficas para o usuário, ao mesmo tempo que transmite a interação através do toque do usuário para o computador.

## Armazenamento e Memória

Toda informação que é transmitida em um computador nada mais é que uma série de oscilações elétricas em seu circuito.

Tais informações podem ser abstraídas para um valor binário. **0** ocorre quando não há energia, **1** ocorre quando há energia.

Toda informação em um computador é uma conversão direta de valores binários presentes na memória deste.

Por exemplo, utilizando do padrão de codificação ASCII (_American Standard Code for Information Interchange_), é possível transmitir a informação `"VIOLET"` convertendo cada letra para binário seguindo a tabela abaixo:

|Valor|  Binário  |Valor|  Binário  |Valor|  Binário  |Valor|  Binário  |
|:---:|:---------:|:---:|:---------:|:---:|:---------:|:---:|:---------:|
|  A  | 0100 0001 |  B  | 0100 0010 |  C  | 0100 0011 |  D  | 0100 0100 |
|  E  | 0100 0101 |  F  | 0100 0110 |  G  | 0100 0111 |  H  | 0100 1000 |
|  I  | 0100 1001 |  J  | 0100 1010 |  K  | 0100 1011 |  L  | 0100 1100 |
|  M  | 0100 1101 |  N  | 0100 1110 |  O  | 0100 1111 |  P  | 0101 0000 |
|  Q  | 0101 0001 |  R  | 0101 0010 |  S  | 0101 0011 |  T  | 0101 0100 |
|  U  | 0101 0101 |  V  | 0101 0110 |  W  | 0101 0111 |  X  | 0101 1000 |
|  Y  | 0101 1001 |  Z  | 0101 1010 |

```elixir
"V" = 0101 0110
"I" = 0100 1001
"O" = 0100 1111
"L" = 0100 1100
"E" = 0100 0101
"T" = 0101 0100

"VIOLET" = 010101100100100101001111010011000100010101010100
```

Da mesma forma é possível traduzir a informação em `"010011000100111101010110010001010100100101010100"` convertendo cada 8 números binários para sua letra correspondente:

```elixir
0100 1100 = "L"
0100 1111 = "O"
0101 0110 = "V"
0100 0101 = "E"
0100 1001 = "I"
0101 0100 = "T"

010011000100111101010110010001010100100101010100 = "LOVEIT"
```

Para o computador, toda informação é um conjunto de valores binários. Essas informações que são utilizadas frequentemente pelo computador precisam estar em um dispositivo de memória que seja rápido o suficiente para não pesar no desempenho do computador.

Geralmente, os dispositivos que utilizam de informações recorrentes possuem uma memória própria chamada de _cache_. Essas memórias armazenam pouquíssima quantidade de informação, mas são rápidas para escrever e ler.

Uma memória central presente na maioria dos computadores é a memória RAM (_random-access memory_), que armazena uma quantidade maior de informação que a _cache_ e são rápidas para escrita e leitura.

Uma memória mais lenta e de alta capacidade de armazenamento está presente na maioria dos computadores através de um disco rígido (_hard disk_, HD).

Tais memórias podem ser divididas em dois tipos:

* **Perene**: Responsável por armazenar grande quantidade de informação à longo prazo ao custo de uma velocidade baixa. Tais informações persistem em um meio físico, não dependendo de energia elétrica. O disco rígido é um exemplo de memória perene.

* **Volátil**: Responsável por armazenar pequena quantidade de informação à curto prazo com uma velocidade de leitura e escrita alta. Tais informações exigem energia elétrica. A memória _cache_ e a memória RAM são exemplos de memória volátil.

## Processador

Processador é o dispositivo responsável por processar instruções em um computador. O termo processador é designado para um tipo específico, chamado de CPU (_central processing unit_).

De forma bem simplista, o processador nada mais é que uma calculadora binária extremamente rápida. Seu ciclo corresponde à três fases:

- ***Fetch***: Fase em que recebe instruções. Tal instrução contém um contador que armazena o endereço de memória da próxima instrução que deve ser recebida. Após receber a informação, o contador é incrementado para a próxima instrução que será recebida, e assim por diante
- ***Decode***: Ao receber a instrução, esta é covertida em sinais operacionais que serão executadas pela CPU
- ***Execute***: Ao decodificar a instrução e emitir os sinais para as partes operacionais da CPU, é realizada a operação e transmitida para o dispositivo de armazenamento.

## O que é _Software_?

_Software_ é um conjunto de instruções que determinam o que o computador deve fazer. Existem vários tipos de _softwares_, que podem ser categorizados de diversas formas.

Uma categorização bem comum é quanto ao propósito do _software_, onde geralmente é dividido entre:

- ***Software*** **de Sistema**: Agem diretamente no _hardware_ do computador, provendo funcionalidades para usuários e outros _softwares_. O sistema operacional (tais como Linux, Windows, iOS ou Android) é um exemplo de _software_ de sistema.
- ***Software*** **de Aplicação**: Agem indiretamente no _hardware_ através de abstrações fornecidades pelo sistema operacional ou outros _softwares_ de sistema. Aplicativos de celulares são exemplos de _softwares_ de aplicação.

Para se construir um _software_, foi desenvolvido linguagens de programação que faz o papel de comunicação entre a pessoa que desenvolve o _software_ e a máquina que irá realizar as instruções.

Certas linguagens possuem alto nível de abstração, objetivando facilitar a escrita de _softwares_. Quanto menor é a abstração, mais próximo é da linguagem de máquina (e mais domínio/liberdade operacional é fornecida, ao custo de uma maior complexidade de escrita).

A linguagem de máquina é aquela utilizada diretamente pelo processador para realizar as operações das instruções.

## Arquivo, Programa e Processo

Um arquivo é um conjunto de dados armazenados em um dispositivo de armazenamento. Arquivos são administrados pelo sistema operacional e organizados de acordo com seu tipo de sistema de arquivos.

Um programa é um tipo de arquivo especial que possui como dado uma coleção de instruções que será executada pelo computador. O navegador é um exemplo de programa. Uma fotografia, entretanto, é apenas um arquivo que pode ser utilizado por um programa capaz de interpretar as informações gráficas contidas nele.

Quando um programa é inicializado, este é convertido em processo. Logo, processo é um programa em execução.

Geralmente, o programa é armazenado em uma memória perene, enquanto o processo é mantido em uma memória volátil.

Ao desenvolver um _software_, o programador cria um arquivo, chamado de código. Este código é convertido em um arquivo de programa por um compilador. Tal arquivo de programa ao ser inicializado cria um ou mais processos que é executado pelo computador.

Há um tipo especial de código, chamado de _script_, que não é convertido em programa. Ao invés disso, um _software_ intermediário, chamado de tradutor, realiza a leitura do _script_ e a executa simultaneamente.
