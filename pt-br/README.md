# Falando Elixir

Bem-vindo ao curso básico de programação "Falando Elixir"!

O perfil esperado para iniciar na seção [Falando em Computador](falando-em-computador):

- [x] Sei pouco sobre como um computador funciona
- [x] Não sei descrever os principais componentes de um computador
- [ ] Quero revisar algum dos seguintes termos/temas:

    - [ ] _Hardware_ & _Software_
    - [ ] Dispositivos de entrada e saída
    - [ ] Armazenamento e memória
    - [ ] Processador

O perfil esperado para iniciar na seção [Falando em Programação](falando-em-programacao):

- [x] Nunca escrevi um código, ou já programei e não tenho segurança para me considerar um programador
- [x] Tenho dificuldades em pensar em soluções para problemas lógicos
- [x] Tenho dificuldades em entender e/ou escrever algoritmos de programação
- [ ] Quero revisar algum dos seguintes termos/temas:

    - [ ] Algoritmos

        - [ ] Função
        - [ ] Variável
        - [ ] Parâmetro
        - [ ] Principais tipos de dados
        - [ ] Módulo
        - [ ] Estruturas condicionais
        - [ ] Estruturas de repetição
        - [ ] Principais estruturas de dados

    - [ ] Solução de Problemas

        - [ ] Técnica da caixa preta
        - [ ] Técnica da segmentação
        - [ ] Técnica da simplificação
        - [ ] Técnica da limitação

    - [ ] Escrevendo Algoritmos

        - [ ] Escrita em diagramas
        - [ ] Escrita em tabela de estados
        - [ ] Escrita em notação funcional
        - [ ] Escrita em pseudo-código

O perfil esperado para inicar na seção [Falando em Elixir](falando-em-elixir):

- [x] Fiz as duas seções anteriores e estou pronto para programar!
- [x] Nunca programei utilizando linguagens funcionais e quero conhecer o paradigma
- [x] Nunca programei em Elixir e quero conhecer a linguagem
- [x] Programei em Elixir mas não tenho segurança com a linguagem
- [ ] Quero revisar algum dos seguintes termos/temas:

    - [ ] Paradigma funcional
    - [ ] Erlang
    - [ ] Scripts em Elixir
    - [ ] Compilação de código em Elixir para BEAM
    - [ ] Tipos de dados em Elixir
    - [ ] Módulos nativos do Elixir e documentação
    - [ ] Estruturas de dados em Elixir
    - [ ] Notação do Elixir
    - [ ] _Pattern Matching_
    - [ ] _Guard clauses_
    - [ ] Principais síntaxes do Elixir
    - [ ] Reuso de código e integração de módulos
    - [ ] OTP e GenServer
    - [ ] Mix
    - [ ] Testes
    - [ ] Recursos importantes do Erlang
    - [ ] _Build_ utilizando Distillery
