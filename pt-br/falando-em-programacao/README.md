# Falando em Programação

Programar é como conversar com a máquina através de cartas. Se algum dia alguém lhe pedir:

> "Escreva uma carta para um brasileiro explicando como fazer um bolo de cenoura."

E esta mesma pessoa pedir para três outras pessoas diferentes, quais as chances das quatro cartas serem idênticas?

Mesmo que todos sigam os padrões de escrita da língua portuguesa, a capacidade intelectual de cada pessoa os diferem ao estabelecer os critérios da melhor forma para escrever como fazer um bolo de cenoura.

Além disso, o próprio domínio do idioma os diferenciam de sua capacidade de escrita. Um alemão que nunca teve contato com a língua portuguesa provavelmente irá recorrer à um tradutor ao invés de tentar escrever a receita em português do pouco que sabe.

Essa natureza intelectual da atividade de programação é o principal fator que o diferencia de um processo de construção tradicional. Cada pessoa tem uma identidade de escrita, pois cada pessoa tem uma identidade de raciocínio.

Outro ponto significativo da escrita é o significado de "melhor". Qual a melhor forma de escrever uma receita de bolo? Enumerar em uma lista, descrevendo minuciosamente os passos? Definir a quantidade dos produtos e as diferentes etapas?

Para uma pessoa que já tenha experiência em confeitaria, palavras-chave seriam suficientes para descrever as etapas.

Para uma pessoa que nunca fez um bolo, é necessário descrever o que cada etapa significa e como executar de forma a minimizar as chances de erros.

Descobrir e entender quem vai ler a carta é o primeiro passo para identficar a melhor forma de redigi-la.

Código é escrito para a máquina, e esta é limitada ao compilador/tradutor que segue as regras de sua linguagem. Um código mal escrito nesse diálogo pessoa-máquina acarreta em um produto inseguro, não-confiável, de _performance_ ruim e diversos outros problemas.

Código também é escrito para pessoas. Um código mal escrito nesse diálogo pessoa-pessoa é um código de difícil leitura, de alta complexidade para modificar e/ou adicionar novas funcionalidades, acarretando em um produto engessado de alto custo de manutenção e evolução.

Portanto, saber escrever o melhor código é saber escrever para pessoas entenderem e modificarem com facilidade ao mesmo tempo que utiliza de algoritmos de alta performance e confiabilidade para o produto ser de alta qualidade.

Com este objetivo, esta seção trata dos seguintes temas:

1. [Algoritmos](#algoritmos)
1. [Solução de Problemas](#solução-de-problemas)
1. [Escrevendo Algoritmos](#escrevendo-algoritmos)

## Algoritmos

Algoritmo é um conjunto de instruções não-ambíguas sobre como realizar uma ou mais tarefas.

A qualidade do algoritmo está diretamente associada à capacidade do mesmo para resolver a(s) tarefa(s) que lhe foi designada.

No contexto de programação, algoritmos são instruções desenvolvidas por pessoas que serão interpretadas pela máquina para a realização de uma ou mais tarefas. Para validar a capacidade de um algoritmo computacional, utilizamos da análise lógico-matemática que infere com confiabilidade o quanto e de que forma o algoritmo realiza a(s) tarefa(s).

Portanto, a própria linguagem de programação possui termos e estruturas parecidas com o universo da matemática.

Aprenderemos a seguir os principais termos e estruturas de um algoritmo computacional, presente nas mais diversas linguagens de programação.

### Função

Na matemática, função é um processo ou relação que associa cada elemento de entrada em um elemento de saída.

Por exemplo, o dobro de um número é uma função que para cada elemento de entrada o elemento de saída é igual ao valor da soma do elemento de entrada com ele mesmo. Algumas formas de descrever essa função:

* Por tabela de entrada e saída:


    |Entrada| Saída |
    |:-----:|:-----:|
    |   0   |   0   |
    |   1   |   2   |
    |   2   |   4   |
    |   3   |   6   |
    |  ...  |  ...  |
    |   x   | x + x |

* Onde o contradomínio (saídas) é igual à operação realizada com o domínio (entradas):


    ```elixir
    y = x + x
    0 = 0 + 0
    2 = 1 + 1
    4 = 2 + 2
    6 = 3 + 3
    ```

    ```elixir
    f(x) = x + x
    f(0) = 0 + 0 = 0
    f(1) = 1 + 1 = 2
    f(2) = 2 + 2 = 4
    f(3) = 3 + 3 = 6
    ```

* Através de instruções literais:

  * **Pegue o valor de entrada, realize a soma dele com ele mesmo.**

* Em pseudocódigo:

    ```
    Função dobro
      entrada: x (tipo: número)
      faça: y (tipo: número) ser igual ao valor x + x
      saída: y
    ```

* Em linguagem de programação C:

    ```c
    int calculate_double(int x)
    {
      return x + x;
    }
    ```

* Em linguagem de programação Python:

    ```python
    def calculate_double(x):
        return x + x
    ```

* Em linguagem de programação Elixir:

    ```elixir
    def calculate_double(x) do
      x + x
    end
    ```

### Variável

Na matemática, variável é um símbolo que representa um número arbitrário.

Por exemplo, na equação da definição de força, `F = ma`, os símbolos `F`, `m` e `a` são variáveis. `F` representa o valor da força, `m` o valor da massa, `a` o valor da aceleração.

No exemplo da função "dobro", `x` e `y` são variáveis.

Na programação, variável é um elemento capaz de armazenar um valor.

Certas linguagens definem variáveis através da declaração do seu tipo. Por exemplo:

* Na linguagem C:

    ```c
    int x; // Declaração da variável 'x' do tipo int (inteiro)
    x = 2; // 'x' recebe valor do tipo inteiro '2'
    x = 3; // 'x' recebe valor do tipo inteiro '3'

    int y = 10; // Declaração da variável 'y' do tipo inteiro com valor '10'
    ```

Outras linguagens definem variáveis sem a necessidade da declaração de seu tipo. Por exemplo:

* Na linguagem Python:

    ```python
    x = 2 # Declaração da variável 'x' com valor '2' (tipo inteiro subentendido)
    x = 3 # 'x' recebe valor '3' (tipo inteiro subentendido)
    ```

* Na linguagem Elixir:

    ```elixir
    x = 2 # Declaração da variável 'x' com valor '2' (tipo inteiro subentendido)
    x = 3 # Redeclaração da variável 'x' com valor '3' (tipo inteiro subentendido)
    ```

Variáveis que não modificam seu valor são chamadas de constantes. Certas linguagens não possuem fornecem síntaxes para declaração de constantes.

Variáveis que são utilizadas como entrada de uma função são chamadas de parâmetros.

Por exemplo:

* Na linguagem C:

    ```c
    float calculate_gravity_force(float mass)
    {
      const float GRAVITY_ACCELERATION =  9.8;
      float force = mass * GRAVITY_ACCELERATION;
      return force;
    }
    ```

    * `mass` é um parâmetro da função `calculate_gravity_force`
    * `GRAVITY_ACCELERATION` é uma constante do tipo `double` (número racional). Não é possível alterar o valor de `GRAVITY_ACCELERATION`
    * `force` é uma variável do tipo `double`. É possível alterar o valor de `force`

* Na linguagem Python:

    ```python
    def calculate_gravity_force(mass):
      GRAVITY_ACCELERATION = 9.8
      force = mass * GRAVITY_ACCELERATION
      return force
    ```

    * `mass` é um parâmetro da função `calculate_gravity_force`
    * `GRAVITY_ACCELERATION` é uma variável. Não existe declaração de constante em Python. Por convenção, variáveis escritas em caixa-alta (em letras maiúsculas) são consideradas como constantes e, portanto, respeita-se a ideia da mesma não ser modificada ao longo do código
    * `force` é uma variável.

* Na linguagem Elixir:

    ```elixir
    @gravity_acceleration 9.8
    def calculate_gravity_force(mass) do
      force = mass * @gravity_acceleration
    end
    ```

    * `mass` é um parâmetro da função `calculate_gravity_force`
    * `@gravity_acceleration` é um atributo de módulo que pode ser utilizado como uma constante. `@gravity_acceleration` não pode ser modificado na função.
    * `force` é uma variável.

### Tipos de dados

Tipos de dados são utilizados na linguagem de programação para identificar e estabelecer critérios de uso e modificação de seus valores. Por exemplo, todas as linguagens de programação ao receber a instrução `1 + 1` realiza a operação de soma, resultando no valor `2`, pois os valores `1` e `1` são do tipo inteiro. Entretanto, se os valores mudarem para `'a'`, a maioria das linguagens de programação realiza a operação de concatenação, resultando no valor `'aa'`, pois os valores `'a'` e `'a'` são do tipo _string_.

Os tipos mais comuns são:

* **Inteiro**: Definidos geralmente como `int`, `integer` ou `Integer`, são números positivos e negativos não-fracionados. Exemplos: `2`, `3`, `-10`

* **Flutuante**: Definidos geralmente como `float` ou `Float`, são números positivos e negativos capazes de serem fracionados. Foi definido com este nome devido à sua forma exclusiva de armazenamento em memória e das operações necessárias para realizações de cálculo. Exemplos: `2.0`, `3.5`, `-25.3`

* **Caractere**: Definidos geralmente como `char` ou `Char`, são definidos como um caractere o valor da tabela ASCII correspondente ao caractere. Exemplos: `'a'`, `'1'`, `'X'`

* _**Boolean**_: Definidos geralmente como `bool`, `boolean` ou `Boolean`, são os valores `0` ou `1`, `true` ou `false`, ou alguma outra representação para seu tipo binário

* **Lista**: Conjunto de um tipo de dado, geralmente definidos por `list`, `array` ou `vector`, e escritos entre colchetes (`[]`). Exemplos: `[0, 2, 4, 6, 8]`, `['o', 'k']`

* _**String**_: Definidos geralmente como `str`, `string`, `String` ou `char[]`, são conjuntos ou listas de caracteres. Exemplos: `"a"`, `"10124"`, `"VIOLET"`, `['o', 'k', '\0']`

### Módulo

Módulo é um conjunto de funções e elementos de seu contexto específico. Por exemplo: podemos definir o módulo `Multiplication` como o conjunto das funções `calculate_double` e `calculate_triple`.

* Em pseudocódigo:

    ```
    Módulo Multiplicação

      Função dobro
        entrada: x (tipo: número)
        faça: y (tipo: número) ser igual ao valor x + x
        saída: y

      Função triplo
        entrada: x (tipo: número)
        faça: y (tipo: número) ser igual ao valor x + x + x
        saída: y
    ```

* Em linguagem Python, o arquivo onde se escreve as funções pode definir um módulo.

* Em linguagem Elixir:

    ```elixir
    defmodule Multiplication do

      def calculate_double(x) do
        x + x
      end

      def calculate_triple(x) do
        x + x + x
      end
    end
    ```

### Estruturas Condicionais

Condição é uma comparação realizada entre variáveis ou entre valores com uma ou mais operações de comparação. Para a maioria das linguagens de programação, é utilizado os seguintes operadores:

* `a == b` (igualdade): Compara se `a` é igual a `b`, retorna verdadeiro (`1`, `True`, `true`) se são iguais. Exemplos: `1 == 1` (true), `1 == 2` (false)
* `a != b` (desigualdade): Compara se `a` é diferente de `b`. Exemplos: `1 != 1` (false), `1 != 2` (true)
* `a > b` (maior que): Compara se `a` é maior que `b`. Exemplos `2 > 1` (true), `4 > 10` (false)
* `a < b` (menor que): Compara se `a` é menor que `b`. Exemplos `2 < 1` (false), `4 < 10` (true)
* `a <= b`, `a >= b` (menor/maior ou igual): Compara se `a` é maior/menor ou igual a `b`. Exemplos `2 >= 2` (true), `5 <= 4` (false)
* `a || b`, `a or b` (ou): Compara se `a` ou `b` possuem valores verdadeiros. Exemplos `false || false` (false), `false || true` (true)
* `a && b`, `a and b` (e): Compara se `a` e `b` possuem valores verdadeiros. Exemplos `false || true` (false), `true || true` (true)
* `!a`, `not a` (não): Compara se `a` não é verdadeiro. Exemplos `!false` (true), `not true` (false)

Lembre-se que `=` é diferente de `==`. `=` significa "receber", "atribuir" e é utilizado para modificar ou atribuir um valor para uma variável. `==` significa "igual a" e é utilizado para comparar valores.

Estruturas condicionais são síntaxes da linguagem que permitem a realização de operações apenas quando certas condições são satisfeitas. As estruturas condicionais mais comuns entre as linguagens são o `if` e `case`.

#### Condição `if` (Se)

A condição `if` agrupa um bloco de código na qual será executado apenas se a condição definida for verdadeira. Esta condição pode possuir um complemento chamado `else` (caso contrário), que é executado quando a condição for falsa. Por exemplo, se eu desejo dobrar um número apenas se este número for ímpar:

* Por tabela de entrada e saída:


    |Entrada| Saída |
    |:-----:|:-----:|
    |   0   |   0   |
    |   1   |   2   |
    |   2   |   2   |
    |   3   |   6   |
    |   4   |   4   |
    |   5   |   10  |

* Onde o contradomínio (saídas) é igual à operação realizada com o domínio (entradas):


    ```elixir
    y = {
      x + x, se (x mod 2) = 1
      x, se (x mod 2) = 0
    }
    0 = 0
    2 = 1 + 1
    2 = 2
    6 = 3 + 3
    ```

    ```elixir
    f(x) = {
      x + x, se (x mod 2) = 1
      x, se (x mod 2) = 0
    }
    f(0) = 0
    f(1) = 1 + 1 = 2
    f(2) = 2
    f(3) = 3 + 3 = 6
    ```

* Através de instruções literais:

  * **Pegue o valor de entrada, se a divisão deste valor por 2 possuir resto 1, realize a soma dele com ele mesmo.**

* Em pseudocódigo:

    ```
    Função dobro
      entrada: x (tipo: número)
      se: resto da divisão de x por 2 possuir valor 1
        faça: y (tipo: número) ser igual ao valor x + x
      senão:
        faça: y (tipo: número) ser igual ao valor x
      saída: y
    ```

* Em linguagem de programação C:

    ```c
    int calculate_double(int x)
    {
      if(x % 2 == 1)
      {
        return x + x;
      }
      else
      {
        return x;
      }
    }
    ```

* Em linguagem de programação Python:

    ```python
    def calculate_double(x):
        if x % 2 == 1:
          y = x + x
        else:
          y = x
        return y
    ```

* Em linguagem de programação Elixir:

    ```elixir
    def calculate_double(x) do
      if rem(x, 2) == 1 do
        x + x
      else
        x
      end
    end
    ```

#### Condição `case` (Caso)

A condição `case` ou `switch case` agrupa blocos de acordo com o valor de uma variável. Por exemplo, se queremos sabe o nome de um dos 4 últimos presidentes do Brasil:

* Por tabela de entrada e saída:


    |Entrada|     Saída     |
    |:-----:|:-------------:|
    |   1   |   Bolsonaro   |
    |   2   |   Temer       |
    |   3   |   Dilma       |
    |   4   |   Lula        |
    |   x   |   Não sei     |

* Em pseudocódigo:

    ```
    Função presidente
      entrada: x (tipo: número)
      caso x:
        seja: 1
          faça: nome (tipo: texto) ser igual ao valor "Bolsonaro"
        seja: 2
          faça: nome (tipo: texto) ser igual ao valor "Temer"
        seja: 3
          faça: nome (tipo: texto) ser igual ao valor "Dilma"
        seja: 4
          faça: nome (tipo: texto) ser igual ao valor "Lula"
        seja: qualquer outro
          faça: nome (tipo: texto) ser igual ao valor "Não sei"
      saída: nome
    ```

* Em linguagem de programação C:

    ```c
    char* president_name(int x)
    {
      switch(x)
      {
        case 1:
          return "Bolsonaro";
        case 2:
          return "Temer";
        case 3:
          return "Dilma";
        case 4:
          return "Lula";
        default:
          return "Não sei";
      }
    }
    ```

* Em linguagem de programação Elixir:

    ```elixir
    def president_name(x) do
      case x do
        1 -> "Bolsonaro"
        2 -> "Temer"
        3 -> "Dilma"
        4 -> "Lula"
        _ -> "Não sei"
      end
    end
    ```

### Estruturas de repetição

Estrutuas de repetição são síntaxes da linguagem de programação que permitem a realização repetitiva de operações enquanto a condição for verdadeira. As estruturas de repetição mais comuns são o `for` e `while`.

#### Condição `for` (para)

A condição `for` realiza as operações de seu bloco ao mesmo tempo que altera valores de variáveis ou o elemento de uma lista. Por exemplo, se eu quero retornar uma lista com o dobro dos números entre 0 e 10, inclusive eles:

* Por tabela de entrada e saída:


    |Entrada| Saída |
    |:-----:|:-----:|
    |   0   |   0   |
    |   1   |   2   |
    |   2   |   4   |
    |   3   |   6   |
    |   4   |   8   |
    |   5   |   10  |
    |   6   |   12  |
    |   7   |   14  |
    |   8   |   16  |
    |   9   |   18  |
    |   10  |   20  |

* Em pseudocódigo:

    ```
    Função tabuada de dois
      entrada: nenhuma
      para: i (tipo: inteiro)
        com: valor inicial 0,
        aumentando: i em valor 1
        enquanto: i possuir valor menor que 11
        faça: y (tipo: lista de inteiros) no índice i possuir o valor i + i
      saída: y
      # saída: [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
    ```

* Em linguagem de programação C:

    ```c
    int i;
    int numbers[11];
    for(i = 0; i < 11; ++i)
    {
      numbers[i] = i+i;
    }
    // numbers = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
    ```

    * Em C, `for` se define através de três partes:

        * **Condição inicial**: Estabelece valores iniciais para zero ou mais variáveis
        * **Condição de repetição**: Estabelece qual critério ele verifica toda vez que terminar o bloco para repetir.
        * **Variação**: Estabelece variações em variáveis a cada fim de uma repetição

* Em linguagem de programação Python:

    ```python
    numbers = []
    for i in range(11):
      numbers.append(x+x)
    # numbers = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
    ```

    * Em Python, `for` é utilizado para percorrer uma lista. No caso acima, `i` é o elemento atual da lista, e `range(11)` é a lista que possui os valores de 0 a 10, inclusive eles

* Em linguagem de programação Elixir:

    ```elixir
    numbers = for i <- 0..10, do: i + i
    # numbers = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
    ```

    * Em Elixir, `for` é utilizado como _list comprehension_, isto é, percorre uma lista para gerar outra lista com os resultados da operação de cada elemento. No caso acima, `i` é o elemento atual da lista, `0..10` é a lista que possui os valores de 0 a 10 (inclusive eles) e `i + i` é a operação para a nova lista.

### Estruturas de Dados

Estruturas de dados são coleções particulares de dados que possuem operações específicas para acesso, relacionamento e modificação. Principais estruturas incluem `array` (vetor), `linked list` (lista encadeada), `stack` (pilha), `queue` (fila), `struct` (também conhecido como `tuple` ou `record`), e `map` (mapa).

#### `array` (Vetor)

Um `array` é uma estrutura de dados que armazena valores de forma agrupada na memória e estes são acessados por índices numéricos. Para criar um `array`, geralmente se define o tamanho (quantidade de elementos a serem armazenados). Este então disponibiliza os índices deste mesmo tamanho para acesso e modificação. Um exemplo de `array` pode ser visto na tabela abaixo:

| 0 | 1 | 2 | 3 | 4 | 5 |
|:-:|:-:|:-:|:-:|:-:|:-:|
|'V'|'i'|'o'|'l'|'e'|'t'|

* Ao acessar o índice `0`, recebe-se o valor `V`.
* Ao requisitar o acesso ao índice `1`, é realizado um "salto" de uma casa do índice `0`, acessando assim o elemento `1` de valor `i`.
* Ao requisitar o acesso ao índice `4`, é realizado um "salto" de quatro casas do índice `0`, acessando assim o elemento `4` de valor `e`.

Exemplos de acesso e modificação de elementos em um `array` em linguagem C:

```c
char name[6] = ['V', 'i', 'o', 'l', 'e', 't'];
char a = name[0]; // 'V'
a = name[2]; // 'o'
name[1] = 'A'; // name = ['V', 'A', 'o', 'l', 'e', 't']
name[3] = name[5]; // name = ['V', 'A', 'o', 't', 'e', 't']
```

Exemplos de acesso e modificação de elementos em um `array` com tamanho dinâmico em linguagem Python:

```python
name = ['V', 'i', 'o', 'l', 'e', 't']
a = name[0] # 'V'
a = name[2] # 'o'
name[1] = 'A' # name = ['V', 'A', 'o', 'l', 'e', 't']
name[3] = name[5] # name = ['V', 'A', 'o', 't', 'e', 't']
```

#### `linked list` (Lista encadeada)

Uma lista encadeada é uma estrutura de dados que possui acesso e modificação de forma semelhante ao `array`. Entretanto, este é capaz de aumentar e diminuir a quantidade de elementos, através de adição e remoção de elementos através do índice ou nas pontas.

Isso acontece através de um encadeamento dos elementos ao adicioná-los. O primeiro elemento possui uma variável que aponta para o endereço de memória do segundo elemento, o segundo para o terceiro, e assim por diante. Desta forma, os elementos não precisam estar lado a lado na memória.

Essa capacidade da lista encadeada possui um custo ao acessar um elemento. No `array`, para acessar um elemento, basta uma operação que é o "salto" para o endereço de memória de acordo com o índice. Na lista encadeada, é necessário `n` operações do percurso na lista sendo `n` o índice do elemento.

#### `stack` (Pilha)

Uma pilha é uma estrutura de dados que age de acordo com o princípio de pilha. Isto é: É possível remover apenas o último elemento adicionado.

Este princípio é conhecido como **LIFO** (_Last In, First Out_ - Último a entrar, Primeiro a sair).

#### `queue` (Fila)

Uma fila é uma estrutura de dados que age de acordo com o princípio de fila. Isto é: É possível remover apenas o primeiro elemento adicionado.

Este princípio é conhecido como **FIFO** (_First In, First Out_ - Primeiro a entrar, Primeiro a sair)

#### `struct`

`Struct` é uma coleção de campos de diferentes tipos de dados, restritos em sua quantidade. Isto é, não é possível adicionar novos campos uma vez definido a `struct`.

Um exemplo de `Struct` que define um `Carro` em Elixir:

```elixir
defmodule Carro do
  defstruct [:marca, :modelo]
end

carro = %Carro{marca: "Fiat", modelo: "Palio"}
```
#### `map` (Mapa)

Semelhante ao `struct`, é uma coleção de campos de diferentes tipos de dados, com a diferença de que não é restrito em sua quantidade.

Um exemplo de mapa em Elixir:

```elixir
carro = %{marca: "Fiat", modelo: "Palio"}
```

## Solução de Problemas

Problema é um termo poderoso que é bastante abordado no nosso dia-a-dia. Utilizamos tal termo, geralmente, quando encontramos alguma barreira para conquistar algum objetivo (ex: Estou com problemas para abrir o navegador) ou quando condenamos o estado de algum objeto/pessoa (ex: Esse computador está com problemas). De modo geral, um problema tende a ser uma situação ou estado indesejado que precisa ser lidado e resolvido.

Entretanto, para a matemática, um problema tem uma conotação mais positiva e explorativa. Um problema na matemática pode ser definido como uma investigação com condições iniciais que possui como objetivo demonstrar um fato ou um resultado. Tal sucinta definição conecta o problema à ação (investigar), ao domínio (condições iniciais) e ao propósito: Como? Quando/Onde/O quê? Para quê?

- O **quando/onde/o quê** é uma observação ou exploração do universo/contexto na qual o problema reside.
- O **para quê** é o motivador e o direcionador para encontrar o resultado adequado para o problema.
- O **como** é respondido através de uma abordagem que se escolhe utilizar para cumprir o propósito com base nas respostas das duas perguntas anteriores.

A resposta para tais perguntas é parte essencial de qualquer técnica ou estratégia de solução de problemas. Esta seção abordará as principais técnicas utilizadas para solucionar problemas computacionais.

### Tentativa & Erro

Talvez a estratégia mais natural que existe, consiste em realizar experimentações com variações até que se encontre o resultado esperado. Informalmente chamado por programadores de _ad hoc_ ou _go horse_.

Tal estratégia é o principal método para a descoberta de antibióticos, onde químicos simplesmente experimentam composições aleatoriamente até encontrar uma que corresponde ao efeito desejado.

Também é o principal método para atletas de esportes ou jogos eletrônicos aprimorarem sua capacidade técnica com o objetivo de vencer campeonatos.

Além disso, a evolução biológica que acontece naturalmente pode ser considerada como resultante de uma estratégia de tentativa e erro. Mutações aleatórias e variações genéticas podem ser consideradas como "tentativas" e a extinção ou incompatibilidade genética/ambiental como "erro".

Algumas características presentes nesta estratégia:

* **Orientada à solução**: Tentativa e erro não tenta descobrir o porquê da solução funcionar, apenas que é uma solução.
* **Específico ao problema**: Tentativa e erro não tenta generalizar a solução para outros problemas
* **Não-ótima**: Tentativa e erro geralmente busca encontrar uma solução, não todas as soluções, tampouco a melhor solução.
* **Simples**: Tentativa e erro pode ser realizada com pouco ou nenhum conhecimento sobre o contexto.

Do ponto de vista de programação, esta estratégia é adequada quando:

* Se pode realizar múltiplas tentativas
* Não se tem conhecimento do contexto do problema e não há margem para aprendizado
* Não se exige uma solução ótima, isto é, de alto desempenho, confiabilidade e que resolva outros problemas semelhantes
* Como primeiro passo para a descoberta de uma solução ótima ou de um processo de aprendizagem

### Pesquisa

Outra estratégia comum, consiste em aplicar ideias existentes para resolver problemas similares. Informalmente chamado por programadores de resolver via _Google_ ou _StackOverflow_.

Entretanto, a qualidade da pesquisa e a complexidade do problema pode exigir mais que uma pesquisa no _Google_ ou no _StackOverflow_, principalmente se o problema não for um problema comum. Desta forma, se faz necessário adaptar o conhecimento adquirido na pesquisa com o objetivo de aproximar à uma solução adequada para o problema. Quanto melhor for o conhecimento prévio, a capacidade de pesquisa e a qualidade do meio de pesquisa, mais confiável será a tentativa de solucionar o problema.

Pesquisa é, sem dúvidas, a estratégia mais utilizada por programadores para resolver problemas computacionais no dia-a-dia.

Do ponto de vista de programação, esta estratégia é adequada quando não se tem conhecimento suficiente para chegar à uma solução e é possível/legítimo/ético pesquisar soluções pré-existentes na _internet_ ou na literatura.

Há uma grande variação de metodologias, processos e técnicas de pesquisas, cada qual com prós e contras. Esta estratégia foi apresentada com o objetivo de desmistificar a pesquisa como uma abordagem viável para solução de problemas. Use-a sempre que for ético utilizar :)

### Prova por Contradição

Consiste em provar que o problema não pode ser resolvido. Desta forma, a prova irá chegar a uma conclusão contraditória ao falhar em provar que não pode ser resolvido, sendo este o ponto de partida para solucionar o problema. É uma abordagem lógica bastante utilizada para validar proposições matemáticas.

Do ponto de vista de programação, esta estratégia é adequada quando:

* Se sabe uma solução genérica para o problema, mas não se tem certeza de que esta solução resolve o problema específico
* Se sabe uma abordagem que pode solucionar o problema mas encontra barreiras para aplicá-lo diretamente
* Existe um ponto de partida para a solução de problema e deseja confirmar que este é um caminho correto

### Redução

Consiste em transformar um problema em outro na qual se saiba a solução. Tal abordagem é bastante conhecida na computação para afirmar semelhança entre um novo algoritmo e algoritmos pré-existentes ou para validar que o algoritmo é uma solução genérica para uma classe de problemas.

Do ponto de vista de programação, esta estratégia é adequada quando:

* O problema que se deseja resolver é similar a um problema que já foi resolvido. Neste caso, uma forma rápida para resolver o novo problema consiste em transformar partes do novo problema em partes do antigo problema, resolver tais partes utilizando da solução conhecida, e utilizá-las para obter uma solução final para o novo problema.
* O problema que se deseja resolver é similar a um problema que já foi apresentado e não foi resolvido ainda. Desta forma, se provarmos que os problemas são parecidos e igualmente difíceis de serem resolvidos, resolver um dos problemas poderá resolver ambos os problemas.

### Dividir para Conquistar

Consiste em dividir um problema grande e complexo em partes menores e solucionáveis. Tal premissa é utilizada como base para uma complexa categoria de técnicas de programação de alta eficiência computacional (de mesmo nome).

Do ponto de vista de programação, esta estratégia é adequada quando:

* É possível separar o problema em problemas menores e mais simples
* A soma das soluções dos problemas menores equivale à solução do problema maior

## Escrevendo Código

A qualidade de um código depende, principalmente, da eficiência deste código em desempenhar a funcionalidade prevista e a facilidade de manutenção. Há diferentes métricas para aferir a qualidade do código, assim como diferentes técnicas para desenvolver algoritmos de alta performance, confiabilidade e de fácil manutenção.

Para contexto de introdução, será apresentado apenas as técnicas elementares que mais impactam na facilidade de manutenção e o básico da análise de complexidade de código.

### Ordem de Grandeza

Um algoritmo pode ser analisado através de como ele se comporta (em quantidade de operações) em razão às variações dos valores de entrada. Como forma de simplificação, abstraímos operação como um fluxo simples que não se repete. Por exemplo:

```python
# Linguagem Python
def double(value):
  return value * 2
```

É uma função que possui um único parâmetro e realiza apenas uma operação. Por mais que eu aumente ou diminua o valor do parâmetro, este não influencia na quantidade de operações da função. Esse tipo de característica em uma função a define como ordem de grandeza constante.

Há uma notação, chamada de "Grande-O" ou "Big-O", que programadores utilizam para estabelecer qual o comportamento do algoritmo quando este tende à valores de entrada altíssimos (mais precisamente, quando os valores tendem ao infinito). A função acima, na notação Big-O, pode ser definida como `O(1)`, pois o custo operacional é constante (não varia com o valor do parâmetro). Entretanto, há casos onde a quantidade de operações varia de acordo com os valores de entrada, por exemplo:

```c
// Linguagem C
int summation(int terms)
{
  int i, sum = 0;
  for(i = 1; i <= terms; ++i)
  {
    sum += i;
  }
  return sum;
}
```

É uma função que possui um único parâmetro e realiza múltiplas operações através de uma estrutura de repetição. Podemos analisar o impacto do parâmetro no custo de operação desta função:

| Valor da Entrada | Número de Operações |  Valor da Saída   |
|:----------------:|:-------------------:|:-----------------:|
|        1         |          1          |         1         |
|        2         |          2          |         3         |
|        3         |          3          |         6         |
|        4         |          4          |         10        |
|        5         |          5          |         15        |
|       ...        |         ...         |        ...        |
|        n         |          n          | 1 + 2 + 3 ... + n |

Quanto maior o valor do parâmetro de entrada, maior será a quantidade de operações, e esse crescimento é proporcional ao valor do parâmetro de entrada. Em notação Big-O, a função `summation` possui complexidade de `O(n)`, pois a quantidade de operações realizadas tende ao valor da entrada.

Uma forma alternativa de encontrar a soma de `n` termos pode ser realizada através da soma de Gauss:

```elixir
# Linguagem Elixir
def summation(terms) do
  (terms * (terms+1)) / 2
end
```

A função acima possui o mesmo parâmetro da função anterior e resulta no mesmo valor da função anterior. Entretanto, sua complexidade é `O(1)`, pois o custo operacional é constante (não varia com o valor do parâmetro).

Utilizar algoritmos que reduz ao máximo o custo de operações é uma boa prática de programação.

### Estilo de Código

O tempo de vida de uma linguagem de programação está diretamente associado à comunidade que ativamente a utiliza. Tal comunidade germina uma cultura que, dentre as características mais importantes, definem regras para escrita de código.

Por exemplo, em Java, métodos e funções são escritos em `camel case` e a chave é aberta na mesma linha da declaração do bloco:

```java
void helloWorld() {
  System.out.println("Hello world!");
}
```

Em C, entretanto, métodos e funções são escritos em `snake case` e a chave é aberta na linha seguinte da declaração do bloco:

```c
void hello_world()
{
  printf("Hello World!\n");
}
```

Escolher uma folha de estilo dentre as mais populares da linguagem é uma boa prática de programação, pois facilita a leitura do código e facilita a definição de um padrão de escrita padronizado entre desenvolvedores.

### Identação

Observe o seguinte código:

```elixir
# Linguagem Elixir
def solve_quadratic_equation(a, b, c) do
if a != 0.0 do
delta = (b*b) - (4*a*c)
if delta == 0.0 do
{:ok, (-b)/(2*a)}
else
{:ok, (-b + :math.sqrt(delta)) / (2*a), (-b - :math.sqrt(delta) / (2*a))}
end
else
:error
end
end
```

A função `solve_quadratic_equation` é simples código que encontra as raízes de uma equação quadrática no padrão `ax² + bx + c = 0`. A leitura do código acima é possível, mas possui uma dificuldade razoável pois os blocos do código não estão bem definidos. Observe agora o mesmo código, porém identado:

```elixir
# Linguagem Elixir
def solve_quadratic_equation(a, b, c) do
  if a != 0.0 do
    delta = (b*b) - (4*a*c)
    if delta == 0.0 do
      {:ok, (-b)/(2*a)}
    else
      {:ok, (-b + :math.sqrt(delta)) / (2*a), (-b - :math.sqrt(delta) / (2*a))}
    end
  else
    :error
  end
end
```

É possível identificar com facilidade aonde começa e termina cada bloco, facilitando bastante a leitura do código. Identar corretamente o código é uma boa prática de programação para qualquer linguagem.

### Nomes

Observe a seguinte função:

```python
# Linguagem Python
def execute(x):
  n = None

  if hasattr(x, 'a'):
    if hasattr(x, 'b'):
      n = x['a'] + ' ' + x['b']
    else:
      n = x['a']
  elif hasattr(x, 'b'):
    n = x['b']

  return n
```

O que essa função faz? Após uma leitura com mais afinco é possível identificar que ela "une" duas strings (devido ao `' '` na soma dos valores do parâmetro `x`), se ambas existirem. Observe agora a mesma função, porém com boa nomenclatura:

```python
# Linguagem Python
def get_full_name(user):
  full_name = None

  if hasattr(user, 'first_name'):
    if hasattr(user, 'last_name'):
      full_name = user['first_name'] + ' ' + user['last_name']
    else:
      full_name = user['first_name']
  elif hasattr(user, 'last_name'):
    full_name = user['last_name']

  return full_name
```

O objetivo da função é evidente, assim como seu comportamento, apenas utilizando de boa nomenclatura para funções, métodos, atributos e variáveis.

### Princípio da Responsabilidade Única

O princípio da responsabilidade única (ou SRP, _single responsibility principle_) é o primeiro dos 5 princípios SOLID para escrita de código. O princípio declara que todo módulo, classe ou função deve possuir uma e apenas uma responsabilidade. Por exemplo, o código:

```elixir
# Linguagem Elixir
def decorate_room(%{} = room, color, shape, object) do
  room =
    case color do
      :red -> Map.put(room, :color, "FF0000")
      :green -> Map.put(room, :color, "00FF00")
      :blue -> Map.put(room, :color, "0000FF")
      _unknown_color -> room
    end

  room =
    case shape do
      :square -> Map.put(room, :dimension, %{edge: 10})
      :circle -> Map.put(room, :dimension, %{radius: 5})
      _unknown_shape -> room
    end

  room_objects =
    if Map.has_key?(room, :objects) do
      Map.get(room, :objects)
    else
      []
    end

  room_objects =
    case object do
      :chair -> ["chair"] ++ room_objects
      :desk -> ["desk"] ++ room_objects
      _unknown_object -> room_objects
    end

  Map.put(room, :objects, room_objects)
end
```

É uma função que adiciona novas informações em um mapa quando a informação é uma informação válida. O mapa servirá para renderizar um quarto em uma interface 3D. A função realiza sua responsabilidade: atualizar as informações do quarto. Porém, internamente, a função:

* Verifica se a cor é uma cor válida e atualiza a cor do quarto caso seja válida
* Verifica se o formato é um formato válido e atualiza o formato do quarto caso seja válido
* Verifica se já existe objetos no quarto, se não existir, cria uma lista. Com a lista de objetos, verifica se o objeto a ser adicionado é um objeto válido. Se for válido, adiciona na lista de objetos. Atualiza a lista de objetos do quarto.

A medida que novas características do quarto são adicionadas para decorar, mais difícil será a leitura do código, a integração de novos comportamentos e a manutenção do sistema. Observe o código abaixo que entrega o mesmo resultado, porém seguindo o princípio de responsabilidade única:

```elixir
# Linguagem Elixir
def decorate_room(%{} = room, color, shape, object) do
  room
  |> colorize_if_valid(color)
  |> change_shape_if_valid(shape)
  |> update_objects_if_valid(object)
end

def colorize_if_valid(%{} = room, :red), do: Map.put(room, :color, "FF0000")
def colorize_if_valid(%{} = room, :green), do: Map.put(room, :color, "00FF00")
def colorize_if_valid(%{} = room, :blue), do: Map.put(room, :color, "0000FF")
def colorize_if_valid(%{} = room, _unknown_color), do: room

def change_shape_if_valid(%{} = room, :square), do: Map.put(room, :dimension, %{edge: 10})
def change_shape_if_valid(%{} = room, :circle), do: Map.put(room, :dimension, %{radius: 5})
def change_shape_if_valid(%{} = room, _unknown_shape), do: room

def update_objects_if_valid(room, :chair), do: Map.put(room, :objects, ["chair"] ++ get_room_objects(room))
def update_objects_if_valid(room, :desk), do: Map.put(room, :objects, ["desk"] ++ get_room_objects(room))
def update_objects_if_valid(room, _unknown_object), do: room

def get_room_objects(%{objects: objects}), do: objects
def get_room_objects(_room), do: []
```

Este novo código facilita a manutenção e evolução de suas funcionalidades ao seguir o princípio da responsabilidade única:

* `decorate_room` mantém sua única responsabilidade de decorar o quarto, redirecionando responsabilidades específicas de cada contexto para seus respectivos métodos.
* `colorize_if_valid` atualiza a informação de cor caso a cor seja válida, e apenas isto.
* `change_shape_if_valid` atualiza o formato do quarto caso o formato seja válido, e apenas isto.
* `update_objects_if_valid` atualiza os objetos do quarto e apenas isso, redirecionando a responsabilidade de pegar a lista de objetos do quarto para o método `get_room_objects`
* `get_room_objects` retorna a lista de objetos do quarto e apenas isto.
